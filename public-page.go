package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

func version() {
	os.Stderr.WriteString(`public-page: v0.2.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: public-page [OPTION] DIR

Options:
	--port -p    port number to be published
	--help       show this help message
	--version    print the version

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

func _main() error {
	var publishPort int
	flag.IntVar(&publishPort, "p", 80, "port number to be published")
	flag.IntVar(&publishPort, "port", 80, "port number to be published")

	var isHelp, isVersion bool
	flag.BoolVar(&isHelp, "help", false, "show this help message")
	flag.BoolVar(&isVersion, "version", false, "print the version")
	flag.Usage = usage
	flag.Parse()

	if isHelp {
		usage()
		return nil
	}
	if isVersion {
		version()
		return nil
	}

	var publishDir string
	if flag.NArg() < 1 {
		return fmt.Errorf("no specify publish dir")
	}
	publishDir = flag.Arg(0)
	_, err := os.Stat(publishDir)
	if err != nil {
		return err
	}
	http.Handle("/", http.FileServer(http.Dir(publishDir)))

	port := fmt.Sprintf(":%d", publishPort)
	return http.ListenAndServe(port, nil)
}

func main() {
	err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "public-page:", err)
		os.Exit(1)
	}
	os.Exit(0)
}
