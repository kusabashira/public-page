public-page
====================
Publish specified directory on localhost

License
====================
MIT License

Usage
====================
	$ public-page [OPTION] DIR

	Options:
		--port -p    port number to be published
		--help       show this help message
		--version    print the version

Example
====================
$ public-page myPage
Publish ./myPage with port ":80" on localhost

$ public-page -p=8080 myPage
Publish ./myPage with port ":8080" on localhost

$ public-page --port=8080 .
Publish current directory with port ":8080" on localhost

Author
====================
Kusabashira <kusabashira227@gmail.com>
